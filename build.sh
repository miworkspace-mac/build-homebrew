#!/bin/bash -x

munki_package_name=homebrew
version=`date +"%Y.%m.%d"`
mkdir -p build-root/usr/local/brew

# grab homebrew
git clone https://github.com/Homebrew/brew.git build-root/usr/local/brew

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} homebrew-${version}.pkg

rm -rf Component-${munki_package_name}.plist

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root homebrew-${version}.pkg ${key_files} --preinstall-script="preinstall.sh" --postinstall_script="postinstall.sh" | /bin/bash > homebrew-${version}.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

# Build pkginfo
plist=`pwd`/homebrew-${version}.plist

# Write installs array
defaults write "${plist}" installs '( { path=/usr/local/brew/bin/brew; type=file; } )'

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/homebrew-${version}.pkg"
defaults write "${plist}" minimum_os_version "10.13.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" display_name "Homebrew Package Manager"
defaults write "${plist}" description "Homebrew open source package manager.  Adds the 'Homebrew Owners' group - users who need to run brew commands must be added to this group via Users & Groups."
defaults write "${plist}" category -string "System Morsel"
defaults write "${plist}" name "Homebrew"
defaults write "${plist}" version "${version}"
defaults write "${plist}" requires '( "Xcode" )'
defaults write "${plist}" qanotes "Use this command in terminal to install the newest version of homebrew: sudo rm -rf /usr/local/brew/bin/brew"

# Make readable by humans
/usr/bin/plutil -convert xml1 "$plist"
chmod a+r "$plist"
