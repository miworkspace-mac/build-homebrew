#!/bin/bash

set -e

# Create homebrew group if it doesn't exist
# we need to test this rather than forcing it through because
# it will delete current members if its forced.
if ! dseditgroup -n . -o read homebrew; then
  dseditgroup -o create -n . -r 'Homebrew Owners' homebrew
fi

