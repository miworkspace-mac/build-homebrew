#!/bin/bash

set -e

chown -R root:homebrew /usr/local/brew
chmod -R g+ws /usr/local/brew

echo /usr/local/brew/bin > /etc/paths.d/homebrew
